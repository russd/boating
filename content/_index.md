---
title: "inQuest Boating: The Boring Details"
---
{{< figure src="IMG_20210812_170840.jpg" width="1200" height="800" caption="mv inQuest in Nashville, TN." >}}

You can read about our travels [here](http://ahoy.inquest.com). Behind the traveling fun and travails is a lot of work, a lot of experimentation, and a lot of refining our approach to living on a boat, both in the stuff we bring along and the way we do things. Not exciting, but maybe of use to others.