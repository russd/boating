+++
title = 'Drinking Water Hose'
date = '2023-04-19'
categories = ['Miscellaneous']
+++
## Drinking Water Hose

Some boaters don't drink water from their tanks. We do. 

We're careful not to tank up with bad water, which hasn't really been an issue. We filter the water filling our tanks. Many recommend not doing this, because you're filtering chlorine, and so risk the water going bad. Many recommend not doing this, but we don't want sediments coming in, and we seem to retain enough chlorine. We live on the boat, so the water gets turned over pretty quickly. We have a watermaker, and that water of course has no chlorine.

We used to use an expensive carbon filter in the kitchen sink for drinking water, but now we filter all house water a second time coming from the tank.

![](images/hose.png)

We use [these](https://a.co/d/3qIXgWn) drinking water hoses. We carry 2, giving us fifty feet, and we have a 15 foot section after our water filter, giving us a bit more range. They last longer, kink less, and our easier to manage than the standard RV/marine drinking water hoses. Our shorter hose is one of [these](https://a.co/d/4SDdNq4), less expensive and similar.





