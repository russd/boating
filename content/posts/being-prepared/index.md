+++
title = 'Being Prepared'
date = '2021-12-19'
categories = ['seamanship']
draft = true
+++
## Being Prepared

Our friends on *One Eye Dog* posted on the [AGLCA](http://greatloop.org) forum:
> Unfortunately I can't share many details at this point, but some friends of ours lost their new to them vessel yesterday in the Bahamas. It belonged to a family of six. They literally lost everything but their passports.  The owner was able to run through the flames to grab them, but only the passports.
> 
>  They have no money, credit cards, cell phones or personal belongings except for the clothes on their backs. 
> 
> The fire was discovered at 3 am when he thought someone was shooting at the boat. When he went outside to check, he discovered the boat was fully engulfed in flames. 
> 
> The reason I'm sharing this is that when we recently obtained our OUPV licenses, our instructor mentioned the importance of fire blankets. He stated that having one in each cabin was extremely important. 
> 
> If someone is in a cabin when fire breaks out, a fire blanket may be the only thing to protect you as you make your way through the salon to safety. We purchased two from Amazon for the One Eye Dog that are in a container that hangs behind the door in the cabin. The blanket can also double for needed warmth. 
> 
> The second thing we discussed was the importance of a ditch bag. We've always put one together for Gulf crossings, but never gave a thought to having one for every day use.
> 
> That will be rectified immediately. All passports, important paperwork including boat registration and insurance info, extra cash, a supply of medications and a light weight set of clothing sealed in a Seal a Meal to make it compact will be included. Everyone will have something personal they may want to add. Our instructor joked about having a copy of War and Peace in his ditch bag. 
> 
> Fire or any other disaster is something we all worry about. Having a fire blanket and ditch bag will help make a horrible situation a little bit better. 

We have fire blankets in each berth. We plan to get a ditch bag (meant to before, but never got around to it). We'll keep it in a sealed exterior compartment--an unused bait box--so we can get to it in an emergency. I've ordered a tool to break the large sealed window for emergency egress.

What we also need is an emergency checklist and periodic drills. Finding the blankets, getting out the door, grabbing the bag, and deploying the dinghy needs to be second nature.
