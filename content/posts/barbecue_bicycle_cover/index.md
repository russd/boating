+++
title = 'The Best Bicycle Cover ...'
date = '2023-04-19'
categories = ['miscellaneous']
+++

## The Best Bicycle Cover isn't a Bicycle Cover...

![](barbecue-cover.jpg)

It's a bbq grill [cover](https://www.amazon.com/gp/product/B086TV77LC/). Seriously. 

Nylon covers for outdoor storage don't hold up well on a boat. The sun is brutal, and nylon bags tend not to make it through a full season.

But, for some reason barbecue covers take the beating much better. And they fit a full sized bike surprisingly well.
