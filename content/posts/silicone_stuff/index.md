+++
title = 'Silicone Galley Items'
date = '2021-09-23'
categories = ['Galley']
+++
## Silicone is your Galley Friend

Collapsible bowls and collanders, measuring cups that won't break, and a sink strainer that stays in place. Kitchen essentials!

### Collapsible Mixing Bowls
Of course the ones we have aren't available now, but [this](https://amazon.com/Squish-Quart-Collapsible-Mixing-Bowl/dp/B07S1CZ4P2/) is pretty close. One 3 qt, one 1.5 qt. We use them daily.

### Collapsible Collanders
[These](https://amazon.com/dp/B07PGMXRHH/) look much like ours. Maybe not every day use, but nearly so.

### Collapsible measuring cups with handles
Something like [these](https://amazon.com/Prepworks-Progressive-Collapsible-Measuring-Cups/dp/B00FOKWT2S/). Differing colors by size helps in selection. We keep them separate, not connected together. We don't use silicone measuring spoons--they take up more space than nesting metal ones, and are a hassle to use.

### Oxo Silicone Measuring Cups
We love [these](https://amazon.com/dp/B01B7HSHMM/). Microwavable, stack together, don't break, and don't rattle. Easy to pick up, but don't grasp directly under the pour spout--they're softer there. ![](images/Oxo.png)
