+++
title = 'mv inQuest Named Storm Plan'
draft= 'true'
date = '2021-12-05'
categories = ['private']
+++
## mv inQuest Named Storm Plan

This plan documents the process by which inQuest will identify, prepare for, and respond to severe weather conditions generally, and specifically for any NOAA Named Storms (Tropical Storms and Hurricanes)

**Condition 0 (Outside Hurricane Season, not Actively Cruising)**
* Monitor local weather
* Maintain awareness of macro weather trends (lows, highs, fronts, winds, temperature, precipitation)
* Monthly review of emergency supplies; replace as needed
* Monthly review cruising plans for next three months with an eye to extreme weather risk
* Follow normal procedures for mooring inQuest securely
  * Secure at dock (ideally crossed bow and stern lines, bow and stern spring lines, minimize risk of lines chafing)
  * Fenders as needed to minimize risk of contacting docks, pilings, or neighboring boats
  * Assure exterior items are secure for typical conditions
  
**Condition 1 (Hurricane Season, not Actively Cruising)**
* Add to/Modify Condition 0 actions:
  * Monitor local weather closely
  * Daily review of weather apps and NOAA Hurricane Center website (https://www.nhc.noaa.gov/)

**Condition 2 (Actively Cruising)**
* Add to/Modify Condition 1 actions:
  * Monitor local weather throughout day
  * Review PredictWind, BuoyWeather, Windy, and weather.gov marine forecasts each morning and throughout the day if concerned
  * Daily review of NOAA Hurricane Center website (https://www.nhc.noaa.gov/)
* Follow normal procedures for mooring inQuest securely
* On Anchor
  * Minimum scope 5:1, 7:1 if significant current/winds, 10:1 if wind forecast >20 kts
  * Boat exterior secured for forecast conditions

**Condition 3 (located within projected path of named storm 5 days or greater)**
* Add to/Modify Condition 3 actions
  * Identify options to move inQuest outside of forecast path, taking into account possible changes in intensity and direction
  * Identify options to haul inQuest onto the hard with tie downs
  * Identify options to move inQuest to hurricane hole
  * Identify options to move inQuest to marina with floating docks and pilings > potential storm surge height
  * Change planned itinerary to preferred option within time constraints

**Condition 4 (located within project path of named storm within 72 hours)**
* Select best option that can be reached by end of day

**Condition 5 (located within projected path of named storm within 48 hours)**
* Secure boat as appropriate at current location
* In all cases (Consistent with forecast severity):
  * Prepare for evacuation
  * Stow all exterior gear in boat or ashore
  * Tape interior of windows
  * Tape seams of doors, windows, and hatches to minimize water intrusion
  * Assure batteries at full charge
  * Close appropriate sea cocks; plug exhaust ports
  
* On shore (Consistent with forecast severity)
  * Secure dinghy separately
  * inQuest chocked and secured with tie down straps connected to securely connected eyes
  
* In Hurricane Hole (Consistent with forecast severity)
  * 100 lb fisherman's anchor deployed in direction of expected greatest force
  * 85 lb Mantus anchor deployed in line with fisherman's anchor or at 90 degrees, depending on local conditions
  * Main rode 2/3 chain, 1/3 1" 3 strand nylon line
  * Secondary rode 30' chain, balance 1" 3 strand nylon line
  * Anchor rodes connected to 30' 3/4" nylon anchor bridles secured to stainless eyes on each hull
  * Rodes connected to chain stopper/deck cleats to back up bridles
  * Depending on local conditions web of additional (minimum 8) lines secured to stern anchor, native trees, pilings, or other points of purchase
  * All deployed lines protected from chafing as needed
  * dinghy secured on shore as appropriate, or if crew remains on boat secured in the tunnel between the hulls (inQuest is a power catamaran), with appropriate fenders/buffering 

* In Marina (Consistent with forecast severity)
  * inQuest moved to widest available slip to maximize distance between boat, pilings, and docks
  * Anchors deployed if appropriate
  * Minimum 8 lines secured to pilings or cleats
  * Maximum 2 lines per cleat (on shore and on boat)
  * 2 150' 1" anchor lines deployed as appropriate
  * All line runs as long as appropriate to accommodate tidal surge
  * All lines protected from chafing as needed
  * All fenders deployed as appropriate
  * Dinghy secured on shore if appropriate, or secured in the tunnel between the hulls as above

**Condition 6 (located within projected path of named storm within 24 hours)**
* Finish securing boat
* Follow any evacuation orders

**Condition 7 (landfall)**
* If not evacuated, tend to boat as conditions dictate
* Ditch bag prepared for immediate exit
* Monitor VHF Channel 16
* run generator as necessary to maintain battery charge for bilge pumps

**Condition 8 (post landfall)**
* If evacuated, return to boat, following civil authority direction
