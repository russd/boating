+++
title = 'Replacing Broken Handles on our Sliding Windows'
date = '2021-12-14'
categories = ['repair']
+++

## Replacing Broken Handles on our Sliding Windows

inQuest has 4 large sliding windows on the upper "Sky Lounge" deck and 5 smaller sliding windows in the galley/saloon on the main deck. They're custom windows from Bomar, now owned by Pompanette.

You open the sliding pane by pulling on a small plastic handle. ![](images/2021-12-16-08-11-54.png) That is, until the handle breaks. ![](images/2021-12-16-08-14-05.png)Then you learn to prise the window open with your finger tips, simultaneously pulling it away from the fixed pane and sideways. It's quite a trick, frustrating and sometimes painful.

Pompanette will provide you replacement handles for a small fee: (part #s for our windows highlighted) 

![](images/2021-12-15-08-27-36.png)


They also provide these directions:
![](images/2021-12-15-08-31-25.png)
The key takeaway from the instructions is you have to remove both panes of glass to remove the broken handle and install the new one. Sound like fun? You betcha!

In addition to the parts you'll need these tools:
* a thin bladed putty knife
* a cable cutter (or an equivalent)
* a flat-bladed screw driver or other small prying tool
* Vise-Grip needle nosed pliers
* a drill with a small bit (fit to pin)
* a rubber mallet

Here's the process that worked for me:
1) from the outside, look at the track containing the fixed window. You'll find a notch in the U-shaped gasket. Pry up the gasket end closest to the window pane (you need to pinch the top edges to release the gasket) Pull that end away from the pane to remove it.
2) Pry up the other gasket end and pop it out of the track. When you get near the top edge of the window pane, cut the gasket and remove that piece.
3) Insert a thin-blade putty knife between the remaining gasket and the fixed pane, and work it around the edge. You're breaking the pane loose from a bead of sealant that keeps it in place.
4) From the inside, do the same on the inner joint of the gasket and the pane.
5) Slide the sliding window and screen open, allowing you to reach the outside of the fixed pane. Press one hand there and the other on the inside of that pane and slide the fixed pane back. You might have to keep digging with the putty knife to loosen the pane from the sealant.
6) When it breaks free, move the fixed pane to the rear of the opening.
7) Pry up the remaining gasket at the bottom, popping it out of the track around the front edge, and pull the top edge forward to free it from the window pane.
8) You can now work the bottom edge of the pane down a bit to free the top edge from the track, tilting it to the outside. Lift and remove.
9) Move the sliding pane and screen forward to expose the seam on the inner track gasket.
10) Use the same approach to remove the inner gasket.
11) Move the sliding pane all the way forward and the screen back. Remove the plastic strip underneath the screen by pulling it forward.
12) Push the bottom of the screen down, tilt the top in, and remove the screen.
13) Slide the pane back, push the bottom down, tilt it to the outside, and remove it from the frame.

Yea! You now have access to the broken handle. Take a break--you've earned it.

14) Lay the sliding pane on a flat surface with the broken handle up.
15) Extract the small metal pin securing the broken handle. I use needle-nose vise-grips.
16) Slide the broken handle up or down the channel to remove
17) Slide the new handle onto the frame. 
18) You'll find a small hole drilled into the frame where the pin was secured. You can either line up the new handle on that hole, drill a hole, and insert the new pin (hard) or align the new handle a bit higher or lower than the broken one's and drill a new hole in the frame.

You're ready to reinstall! 
19) From the outside, tilt the bottom of the sliding window into the inner channel. The top will clear the upper channel lip.
20) Reinstall the front inner channel gasket.
21) Move the sliding window forward into the gasket.
22) Insert the bottom edge of the screen at the rear of the window. Press it down and the upper edge will clear the track. Insert the plastic strip under the screen. Push the screen forward.
23) Insert the rear inner gasket section, sliding the top section over the sliding pane.
24) Insert the bottom gasket section.
25) From the outside, put the fixed pane bottom into the outer track. Press it down and the top will clear the upper track.
26) Move it to the rear; insert the outer gasket front section, sliding the upper piece over the fixed pane.
27) Move the fixed pane forward onto the front gasket.
28) Install the rear outer gasket. Move the fixed pane back onto it.
29) Run a bead of silicone caulk into the front outer gasket.
30)  Insert the bottom outer gasket under the fixed pane.
31) Move the fixed pane forward, seating it into the caulk. Clean up any excess. Avoid getting it into the inner track/gasket.

You're done! Let the silicone caulk cure before pulling back on the fixed pane.
