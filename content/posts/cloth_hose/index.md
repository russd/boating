+++
title = 'Non-drinking Water Hose'
date = '2023-04-19'
categories = ['Boat Cleaning']
+++
## Hose (Not for drinking water)

You'll need a hose for washing down the boat and similar non-drinking-water purposes. We like this [one](https://a.co/d/fOadpLU)

It holds up well, doesn't develop permanent kinks, and is easy to handle and stow. 50' works for us. 
