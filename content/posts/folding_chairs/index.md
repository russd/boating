+++
title = 'Folding Chairs'
date = '2021-10-11'
categories = ['Miscellaneous']
+++

## Folding Chairs

Docktails provide loopers a social foundation--it's a long trip, and hanging out with others making it helps a lot. For example it gives you a chance to test your stories on a sympathetic and informed audience before posting to your blog or Facebook. 

You'll need to provide some go-to snacks, your beverage of choice, and often a place to sit. Folding camp chairs are popular, but most are bulky, difficult to stow, and awkward to carry. [These](https://www.amazon.com/GCI-Outdoor-Compact-Folding-Chair/dp/B07N716W1G/) chairs fold up into neat ![rectangles](images/pico-folded.jpg) in an easy to carry bag, don't weigh a ton, and provide a comfortable, secure ![seat](images/pico.jpg). Highly recommended.