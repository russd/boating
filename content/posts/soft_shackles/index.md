+++
title = 'Soft Shackles'
date = '2023-04-19'
categories = ['Boat Tackle']
+++

## Soft Shackles

![](images/soft%20shackle.png)

I'm a huge fan of soft shackles: ingeniously knotted loops of dyneema that can replace metal shackles in many situations. They're incredibly strong, easy to handle, and don't make noise or mar as they bang against your gel coat or other hard surfaces.

### How strong are they?
It depends. There are multiple techniques for tying soft shackles, all quite recent. All are stronger than the Dyneema line used to make them, which is not the way knots normally go. The first design was 1.5+ as strong; the latest and most widely used is nearly 2.5 times as strong. The details are interesting, if you're interested in such things, and you probably aren't. For the few that are, lots of online articles and videos await.

### What we use them for

#### Connecting Bridles
Our power cat has stainless steel eyes about a foot above the waterline on the bow of each hull. Normally you'd use a stainless steel shackle to connect the bridle to each eye. Instead I use [these](https://amazon.com/gp/product/B08LK3XQ1Q/). 41K lbs breaking strenth. 
As captain for your boat you need to determine a safety factor to calculate the safe working load for tackle. We target our anchoring gear for about 3K lbs--a compromise given the large wind surface inQuest presents and our desire to stay secure in very high winds (like the 50 kt sustained and 80 kt gusts we experienced on anchor in Destin harbor spring 2021) on one side, and how oversized our ground tackle is compared to other boats our size on the other. These shackles provide ~10K lbs at a safety factor of 5 (for example), higher than the SWL of our chain, the anchor swivel, and our 3/4" 3 strand nylon bridles. 

We replace these shackles yearly, since they're exposed to sunlight and the water environment every day. I haven't tested the old ones, but my guess is they remain stronger than the other tackle and would remain so for multiple seasons.

#### Dinghy Spring Lines
We use smaller soft shackles I tie myself to connect lines to our dinghy to minimize swinging on our davits. They're easy to add and remove, allowing the lines to be used for other purposes.

#### Securing stuff to rails
Bikes, scooters, ladders--a soft shackle around the rail, another around the item, connected through each other. Or as an anchor on the rail for some other lashing. Easy to open and close.

### Make Your Own
If you like to work with knots tying a soft shackle is very challenging and consequently very satisfying when you succeed. You'll need some specialized kit and some tutorials. The hard bit is tying the button knot (key to achieving the highest strength). It's a bear to learn, though of course pretty straightforward once you get it (until you forget and have to relearn). My favorite tutorial is this [one](https://www.youtube.com/watch?v=DYF92CfLD_0).