+++
title = 'Instant Pot Pressure Cooker'
date = '2023-04-17'
categories = ['Galley']
+++
## Our most Essential Kitchen Appliance

Hand's down it's the Instant Pot. ![](images/instantpot.jpg) You can buy them everywhere. We use it to make yogurt, rice, oatmeal, and beans mostly. All are staples for us. We have the 6 quart model, big enough for a gallon of milk, 3 cups of oatmeal, 3 cups of rice, or a pound of beans. 

- We're particular about our greek yogurt, and sometimes local groceries stock brands we don't like. But most stock gallons of milk, and the rest is on us. 
- Pressure cooking steel-cut oats is really easy. We do rolled oats. You need a glass bowl that fits in the Instant Pot, put it on the trivet, and pressure cook for 7 minutes. Cleanup is minimal.
- We buy rice in bulk, vacuum seal 3 cup portions, and so always have rice on hand. 
- We eat red and white beans, and black-eyed peas often (we prefer Camellia brand, which happens to people that live in New Orleans). With fresh beans you really don't need to soak at all; if they've been around a while soaking for an hour with hot water and a bit of baking soda does the trick.

We use it for other one-pot recipes, but just these items pay its freight.

