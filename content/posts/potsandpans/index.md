+++
title = 'Pot and Pan Set'
date = '2021-09-24'
categories = ['Galley']
+++
## Nesting Pots and Pans
![](images/Magma.png)
We first bought these when we had an RV with an induction cooking plate. We moved them to the boat, and along with a fry pan they're all we use on the cooktop. High quality. We also use these pot protectors. ![](images/protectors.png) Available on amazon and elsewhere.