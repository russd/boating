+++
categories = ['Navigation Aids']
title = 'Binoculars'
date = '2023-04-19'
+++

## Binoculars

The topic comes up on the online forums frequently. Here's our view: (hah)

### Why do you need binoculars?
After using binoculars regularly traveling by boat I've comed to think they should be issued at birth and always at hand. We're constantly almost seeing something and want to see it better.

#### Can I clear that bridge?
OK, we find binoculars crucial for one use in particular: seeing the gauge on bridges. We know our air draft:
* as we normally run, with all antennas deployed
* if we lower our VHF antennas but leave the radar mast up
* if we lower the radar mast

It takes a minute to lower the VHF antennas. It takes another minute to lower the radar mast. Raising them is similar. We'll happily spend that time to avoid waiting on a bridge to open that we can clear by lowering. It turns out that requesting a bridge to open that you can clear is illegal:

> § 117.11 Unnecessary opening of the draw. No vessel owner or operator shall— (a) Signal a drawbridge to open if the vertical clearance is sufficient to allow the vessel, after all lowerable nonstructural vessel appurtenances that are not essential to navigation have been lowered, to safely pass under the drawbridge in the closed position; or (b) Signal a drawbridge to open for any purpose other than to pass through the drawbridge opening.
[CGD 91-059, 59 FR 16563, Apr. 7, 1994]

Many (but not all) bridges have a water level tide board that allows you to know the bridge clearance at the current water level. These boards can be hard to spot and hard to read. Binoculars help, a lot. If the bridge height is in the ballpark of our air draft we'll wait until we can read the board before calling to request an open. 

Many bridge operators won't tell you the clearance on the radio--I assume they're worried about liability if things go wrong. 

You also have to take into account whether the indicated height is to the center or to the low point of the navigable span. That little sign can also be hard to spot. One other note: it's not unusal to find cables, scaffolding, or the like that affects the clearance but isn't accounted for by the board. When in doubt proceed slowly, prepare to stop, or just request an opening.

#### Can I enter that lock?
Many locks have a stoplight on the approach wall: red, yellow, and green. If they blew the whistle indicating you can enter but you missed it, you can proceed if you see a green light. Yellow typically means they're in the process of raising or lowering. Spotting the light can be tricky, and binoculars help. If we've been told we can enter but the light isn't green we'll confirm with the lock master.

#### What's the name of that boat?
We love AIS, and wish every boat would broadcast an AIS signal. Since many don't, it's nice to get a name or some detail to use to hail them. 

#### What's the color/shape/number of that ATON?
In some lighting it's tough to tell red from green and square from triangle, and in some situations a little confusion can have major consequences. 

#### Where is the fuel dock? Dock E? Slip 17?
We don't like wandering around marinas trying to find our destination. Some marinas provide maps and clear instructions. Many don't. 

### Our recommendation: Image stabilizing binoculars
In particular we like the Fujinon Techno-Stabi 16 x 28 Compact Binoculars. You can find them at all the usual sources.




