+++
title = 'Dish Drying Rack'
date = '2023-04-19'
categories = ['Galley']
+++
## Dish Drying Rack

There's a huge variety of these things online, and the best one for your boat will depend on the details. We're happy with [this one](https://a.co/d/evNOtz7)
![](images/dishrack.png)