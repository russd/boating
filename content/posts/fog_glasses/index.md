+++
title = 'Tinted Glasses for Foggy Days'
date = '2023-04-17'
categories = ['Navigation Aids']
+++

## Tinted Glasses for Foggy Days

We probably run in fog more often than most boaters. We have doppler radar, an automated fog horn, and two sets of eyes to keep watch. When it's really thick I frequently go to the bow to help spot ATONs and listen for other boaters with the bad judgment to be in the fog. 

Both of us wear [these](https://a.co/d/b3DKYp3)
![](glasses.png)

In our estimation they make a significant difference--they improve contrast, making it easier to pick objects out of the haze. It's not magic, but it helps. And the color adds cheer to a gloomy day.
