+++
title = 'Dyneema Webbing Spool'
date = '2023-04-16'
categories = ['Boat Tackle']
+++
## Dyneema Webbing for Shore Tie/Stern Anchoring

When people tour inQuest they frequently ask what this ![](images/ultra-ultraline-dyneema.jpg) is. It's a spool of Dyneema webbing, mounted on our starboard rail near the stern. We use it for tying to shore, holding the boat between two dolphins, stern anchoring, and the like.

We saw a similar product on an Endeavour 44 when we first started looking for a boat. That one had a plastic spool and cost quite a bit less. Unfortunately it was made in England and I could never find a supplier.

Instead I bought ours from Ultra Marine, purveyor of high quality, shiny, stainless steel anchors and accessories. And well know for being priced for the high end yacht market. I paid over a thousand for our spool with 200 ft of dyneema. I'm guessing we've used it 10 times, so we're down to $100 per use. 

Sadly that unit now sells for [1560 euros](https://www.ultramarine-anchors.com/ultraline-dyneema), or ~$1700 at today's favorable exchange rate. You'll need to use it nearly twice as often to get your per use cost equivalent to ours.

On the other hand you'll get plenty of questions about it...

If you can find the cheaper alternative I'd highly recommend it. We'd have used it more often but only realized afterwards that we missed the chance. 

