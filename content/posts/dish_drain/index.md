+++
title = 'Kitchen Sink Drain Stopper'
date = '2023-04-19'
categories = ['Galley']
+++
## Dish Sink Stopper

In the RV world one has a gray tank along with the black tank. In the boating world by and large we just drain sink and shower water overboard. Helps explain the water in some marinas with  low or no current.

It's a great idea to minimize the amount of grease and food scraps that make it into our water supply. The best drain stopper we've found is [this](https://youtu.be/nN70Y9rCy1E). It's silicone, effective, easy to clean, and fits our non-standard sink drain.
![](images/dishdrain.png)
