+++
title = 'Pump Out Adapter'
date = '2023-04-19'
categories = ['Miscellaneous']
+++

## Pump Out Adapter
Our boat has 2 black water holding tanks, one on each side of the bow. Pumping out requires being on the boat to move the hose to the far side. Many dockhands aren't comfortable boarding your boat, and many pump out stations are self-serve. Anyway it's typically faster and easier if I handle the working end and the dockhand deals with the pump, handling the hose, priming, etc. 

No matter who's handling the working end, pumps work better with a good seal. It turns out there are 2 common sizes of pump out fittings, and one of the sizes has 2 different thread counts, so rather than find the right fitting for your boat, most pumpouts have a "one size fits all" tapered flexible tip for the hose. You press it down until it seals and hold it steady while you pump out.

It's a bother--one little slip and you let air into the system, losing suction and potentially losing prime. As they age and wear they lose flexibility and become harder to seal. Pumping out isn't a particularly fun task, and taking more time doesn't improve it.

We carry the right adapter for our boat with us. ![](images/pumpout%20adapter.png)It's easy to spin ours in, remove their adapter from the hose and clamp onto ours. No searching for the right adapter, no need to maintain manual pressure on the fitting--just get it done.

When you're finished, give it a rinse and return it to its well-known and convenient storage location, and you're ready for the next time.

The variants are listed below, with the Sealand (now Dometic) part number. You need to identify the right one for your boat. You can find them on Amazon, Defender.com, and elsewhere.




    1-7/8" - 11.5 threads per inch = 310343502
    1-5/8" - 11.5 threads per inch = 310343503
    1-5/8" - 16 threads per inch = 310343504
