+++
title = 'Electric Scooters'
date = '2023-04-19'
categories = ['Going Ashore']
+++
## Electric Scooters

We've tried electric bikes and find them just heavy enough to discourage regular use. What's worked best for us is electric scooters. They come in a range of speeds, weights, and cost. Ours weigh about 40 lbs, pretty much the limit we want to transfer from boat to dock or dinghy. 

They're particularly useful expanding your walking range in urban areas or small towns. We've used them in very rural/remote areas as well. Small two lane roads with no berm and no sidewalk leave you depending on alert and cautious drivers. The same is true with bikes, but maybe the surprise factor is a bit less. Not that walking is much better. The US is just amazingly car-centric....
